from distutils.core import setup, Extension
from Cython.Build import cythonize

# aevol path since the place the ihm.py is running
# compile with : python3 setup.py build_ext --inplace in this same place
aevol_path="/home/ambre/Bureau/Aevol/aevol/"
setup(ext_modules=cythonize(Extension(
        "cython_Interaction",
        sources=["cython_Interaction.pyx", aevol_path+"src/libaevol/7/Interaction.cpp"],
        language="c++",
        include_dirs = [aevol_path+'src/libaevol', aevol_path+'src/libaevol/7', aevol_path+'src/libaevol/biochemistry', aevol_path+'src/libaevol/cuda', aevol_path+'src/libaevol/io', aevol_path+'src/libaevol/mutation', aevol_path+'src/libaevol/phenotype', aevol_path+'src/libaevol/population', aevol_path+'src/libaevol/raevol', aevol_path+'src/libaevol/rng', aevol_path+'src/libaevol/stats', aevol_path+'src/libaevol/utils', aevol_path+'src/libaevol/world'],
        extra_objects=[aevol_path+"build/shared/src/libaevol/libaevol.so"],
        extra_compile_args = ['-O0', '-Wall', '-fPIC', '-shared', '--std=c++14', '-DBASE_2', '-DSFMT_MEXP=607'],
        library_dirs=[aevol_path+"build/shared/src/libaevol"],
        runtime_library_dirs=[aevol_path+"build/shared/src/libaevol"]
)))
