# ************************************************************************
#                          JsonToPython  -  realization
# ************************************************************************
# Transform each json files datas into python array usable from the python 
# classes.
# ---------------------------------------------------------------- INCLUDE
import json

def serialize_repartition (file_name):
    file=open(file_name,)
    repartition = json.load(file)
    file.close()

    ncol = repartition["ncol"]
    fitness_matrix = repartition["fitness_matrix"]
    return ncol, fitness_matrix

def serialize_fitness_evolution (file_name):
    file=open(file_name,)
    fitness_evolution = json.load(file)
    file.close()
    target = fitness_evolution["target"]
    point_x = fitness_evolution["point_x"]
    point_y = fitness_evolution["point_y"]
    return (target, point_x, point_y)

def serialize_proteins (file_name):
    file=open(file_name,)
    proteins = json.load(file)
    file.close()
    length = proteins["length"]
    leading_prots = proteins["leading_prots"]
    lagging_prots = proteins["lagging_prots"]
    return (length, leading_prots, lagging_prots)

def serialize_rnas (file_name):
    file=open(file_name,)
    rnas = json.load(file)
    file.close()
    length = rnas["length"]
    leading_rnas = rnas["leading_rnas"]
    lagging_rnas = rnas["lagging_rnas"]
    return (length, leading_rnas, lagging_rnas)