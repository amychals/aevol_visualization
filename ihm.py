# ************************************************************************
#                          ihm  -  Main
# ************************************************************************

# ---------------------------------------------------------------- INCLUDE
import Visualization as vs
import Save as sv
import Listener as react
from cython_Interaction import PyInteraction

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.widgets import Button
import signal
from matplotlib import gridspec
import sys
from pathlib import Path
import getopt

# matplotlib embedded on a tkinter window (to manage the buttons)
matplotlib.use("TkAgg")

# ------------------------------------------------------- Global variables
pause = False
display = False
save_path = './Image/'
# -------------------------------------------------------------- Functions
# Button "Pause" associated function
def pause_reaction (event):
    global pause
    print("Click on pause", pause)
    pause ^=True

# Button "Display visualization" associated function
def display_reaction (event):
    print("Click on display visualization")
    global display
    display = not display

# Button "Display visualization" associated function
def quit_reaction (event):
    print("Click on quit")
    print("End of simulation !")
    print("Saving figure in "+save_path)
    Path(save_path).mkdir(parents=True, exist_ok=True)
    fig.savefig(save_path+'figures.svg', format='svg')
    fig.savefig(save_path+'figures.png', format='png')

    sv.saveFitnessEvolutionFromJson('./json/fitness_evolution_fic.json', save_path)
    sv.saveProteinsFromJson('./json/proteins_fic.json', save_path)
    sv.saveRnasFromJson('./json/rnas_fic.json', save_path)
    plt.close('all')

    sys.exit(0)

# Ctrl+C allow to quit the simulation and save the images in .svg and .png 
# thanks to this signal handler (using SIGINT signal)
def handler(signum, frame):
    print("End of simulation !")
    print("Saving figure in "+save_path)
    Path(save_path).mkdir(parents=True, exist_ok=True)
    fig.savefig(save_path+'figures.svg', format='svg')
    fig.savefig(save_path+'figures.png', format='png')

    sv.saveFitnessEvolutionFromJson('./json/fitness_evolution_fic.json', save_path)
    sv.saveProteinsFromJson('./json/proteins_fic.json', save_path)
    sv.saveRnasFromJson('./json/rnas_fic.json', save_path)

    plt.figure().clear('all')
    plt.close('all')
    plt.cla()
    plt.clf()

    exit(0)

# ------------------------------------------------------------------- MAIN

signal.signal(signal.SIGINT, handler)

if(len(sys.argv)==1):
    print("Parameters file required")
    exit(0)
print("Début de la simulation : ")
Path("./json").mkdir(parents=True, exist_ok=True)
# Instanciate the Interaction object that communicate with the c++ wrapped
# module (glue with cython)
interact = PyInteraction()

# Path of the parameters json file of the simulation 
argv = sys.argv[2:]
try:
    opts, args = getopt.getopt(argv, "o:")
except:
        print("Error")

for opt, arg in opts:
    print(opt, arg)
    if opt in ('-o'):
        save_path = arg

parameters_file = sys.argv[1]

# Path for saving figures
Path(save_path).mkdir(parents=True, exist_ok=True)
# Instanciate the differents objects for the simulation
PyInteraction.setUp(interact, parameters_file.encode('UTF-8'), 100, 0)
# Number of the generation
i=1
print("Generation",i)
PyInteraction.runAStep(interact)
i+=1
print("Generation",i)
# Path of the json file to read in order to plot the differents graphics
repartition_fic_name = './json/repartition_fic.json'
evolution_fic_name = './json/fitness_evolution_fic.json'
proteins_fic_name = './json/proteins_fic.json'
rnas_fic_name = './json/rnas_fic.json'

# ----------Initialisation of the main figure and the axes of the subplots

fig, ax=plt.subplots(2, 2, figsize=(20,10), gridspec_kw={'height_ratios': [1, 1.5]})
fig.subplots_adjust(left=0.0, bottom=0.05, right=0.95, top=0.89, wspace=0.1, hspace=0.15)
plt.ion()   # allow the real time changes

# Initialisation of the differents graphics
repartition_array = vs.compute_array_repartition(repartition_fic_name)
(im)=vs.init_repartition(repartition_array, fig)

(target, point_x, point_y) = vs.compute_array_fitness_evolution(evolution_fic_name)
vs.init_fitness_evolution(fig, target, point_x, point_y)

(length, leading_prots, lagging_prots) = vs.compute_array_proteins(proteins_fic_name)
vs.init_proteins(fig, length, leading_prots, lagging_prots)

(length, leading_rnas, lagging_rnas) = vs.compute_array_rnas(rnas_fic_name)
vs.init_rnas(fig, length, leading_rnas, lagging_rnas)


# Pause button 
ax_pause_button = plt.axes([0.87, 0.14, 0.1, 0.055])
butt_pause = Button(ax_pause_button, 'Pause',color="White")
butt_pause.on_clicked(pause_reaction)

# Display visualization button 
ax_display_button = plt.axes([0.87, 0.08, 0.1, 0.055])
butt_display = Button(ax_display_button, 'Display Visualization',color="White")
butt_display.on_clicked(display_reaction)

# Quit button 
ax_quit_button = plt.axes([0.87, 0.02, 0.1, 0.055])
butt_quit = Button(ax_quit_button, 'Quit',color="White")
butt_quit.on_clicked(quit_reaction)

# Number of Generation text
(x_big_tx, y_big_tx)=(0.3, 0.35)
ax_nbgeneration = fig.add_axes([0.15, 0.9, 0.2, 0.1])
ax_nbgeneration.axis('off')
ax_nbgeneration.text(x_big_tx, y_big_tx, 'Generation ', fontsize = 18)
txt_nbgeneration = ax_nbgeneration.text(0.74, 0.35, str(1), fontsize = 18)

# Fitness of the best individual text
ax_fitness = fig.add_axes([0.15, 0, 0.2, 0.1])
ax_fitness.axis('off')
ax_fitness.text(x_big_tx, y_big_tx, 'Fitness of the best indiv is', fontsize = 14)
txt_fitness = ax_fitness.text(1.08, 0.35, str(PyInteraction.get_BestIndivFitness(interact)), fontsize = 14)

# Size of the genome text
ax_length = fig.add_axes([0.15, 0, 0.2, 0.1])
ax_length.axis('off')
ax_length.text(0.3, 0.1, 'Length of genome is', fontsize = 11)
txt_length = ax_length.text(0.8, 0.1, str(length), fontsize = 11)

# ------------------------------------------ Change Mutations Rates window
# create a new figure (window)
interactFig = plt.figure(2, figsize=(5,5))  
ax_text = interactFig.add_axes([0.45, 0.05, 0.15, 0.65])
ax_text.axis('off')

# add the texts
txt_list = [0]*7
txt_list[0] = ax_text.text(0.2, 0.01, s=str(react.get_DuplicationRate(interact)), fontsize = 11)
txt_list[1] = ax_text.text(0.2, 0.17, s=str(react.get_DeletionRate(interact)), fontsize = 11)
txt_list[2] = ax_text.text(0.2, 0.32, s=str(react.get_TranslocationRate(interact)), fontsize = 11)
txt_list[3] = ax_text.text(0.2, 0.48, s=str(react.get_InvertionRate(interact)), fontsize = 11)
txt_list[4] = ax_text.text(0.2, 0.63, s=str(react.get_PointMutationRate(interact)), fontsize = 11)
txt_list[5] = ax_text.text(0.2, 0.78, s=str(react.get_SmallInsertionRate(interact)), fontsize = 11)
txt_list[6] = ax_text.text(0.2, 0.94, s=str(react.get_SmallDeletionRate(interact)), fontsize = 11)

# add the button axes (location box)
ax_incrDup_button = plt.axes([0.6, 0.05, 0.05, 0.05])
ax_decrDup_button = plt.axes([0.4, 0.05, 0.05, 0.05]) 
ax_incrDel_button = plt.axes([0.6, 0.15, 0.05, 0.05])
ax_decrDel_button = plt.axes([0.4, 0.15, 0.05, 0.05])
ax_incrTrans_button = plt.axes([0.6, 0.25, 0.05, 0.05])
ax_decrTrans_button = plt.axes([0.4, 0.25, 0.05, 0.05]) 
ax_incrInvert_button = plt.axes([0.6, 0.35, 0.05, 0.05])
ax_decrInvert_button = plt.axes([0.4, 0.35, 0.05, 0.05]) 
ax_incrPoint_button = plt.axes([0.6, 0.45, 0.05, 0.05])
ax_decrPoint_button = plt.axes([0.4, 0.45, 0.05, 0.05]) 
ax_incrSmIns_button = plt.axes([0.6, 0.55, 0.05, 0.05])
ax_decrSmIns_button = plt.axes([0.4, 0.55, 0.05, 0.05]) 
ax_incrSmDel_button = plt.axes([0.6, 0.65, 0.05, 0.05])
ax_decrSmDel_button = plt.axes([0.4, 0.65, 0.05, 0.05]) 

# add the buttons in theirs respectives boxes
butt_incrDup = Button(ax_incrDup_button, label='+',color="White")
butt_decrDup = Button(ax_decrDup_button, label='-',color="White")
butt_incrDel = Button(ax_incrDel_button, label='+',color="White")
butt_decrDel = Button(ax_decrDel_button, label='-',color="White")
butt_incrTrans = Button(ax_incrTrans_button, label='+',color="White")
butt_decrTrans = Button(ax_decrTrans_button, label='-',color="White")
butt_incrInvert = Button(ax_incrInvert_button, label='+',color="White")
butt_decrInvert = Button(ax_decrInvert_button, label='-',color="White")
butt_incrPoint = Button(ax_incrPoint_button, label='+',color="White")
butt_decrPoint = Button(ax_decrPoint_button, label='-',color="White")
butt_incrSmIns = Button(ax_incrSmIns_button, label='+',color="White")
butt_decrSmIns = Button(ax_decrSmIns_button, label='-',color="White")
butt_incrSmDel = Button(ax_incrSmDel_button, label='+',color="White")
butt_decrSmDel = Button(ax_decrSmDel_button, label='-',color="White")

# add a reaction function when clicking on each button
butt_incrDup.on_clicked(react.incrInteract(interact, 'dup', txt_list, ax_text))
butt_decrDup.on_clicked(react.decrInteract(interact, 'dup', txt_list, ax_text))
butt_incrDel.on_clicked(react.incrInteract(interact, 'del', txt_list, ax_text))
butt_decrDel.on_clicked(react.decrInteract(interact, 'del', txt_list, ax_text))
butt_incrTrans.on_clicked(react.incrInteract(interact, 'trans', txt_list, ax_text))
butt_decrTrans.on_clicked(react.decrInteract(interact, 'trans', txt_list, ax_text))
butt_incrInvert.on_clicked(react.incrInteract(interact, 'inv', txt_list, ax_text))
butt_decrInvert.on_clicked(react.decrInteract(interact, 'inv', txt_list, ax_text))
butt_incrPoint.on_clicked(react.incrInteract(interact, 'point', txt_list, ax_text))
butt_decrPoint.on_clicked(react.decrInteract(interact, 'point', txt_list, ax_text))
butt_incrSmIns.on_clicked(react.incrInteract(interact, 'smins', txt_list, ax_text))
butt_decrSmIns.on_clicked(react.decrInteract(interact, 'smins', txt_list, ax_text))
butt_incrSmDel.on_clicked(react.incrInteract(interact, 'smdel', txt_list, ax_text))
butt_decrSmDel.on_clicked(react.decrInteract(interact, 'smdel', txt_list, ax_text))

# add the unmoving texts
plt.text(-0.75, -1.35, 'Duplication rate', fontsize = 11)
plt.text(-0.75, -1.13, 'Deletion rate', fontsize = 11)
plt.text(-0.75, -0.90, 'Translocation rate', fontsize = 11)
plt.text(-0.75, -0.68, 'Invertion rate', fontsize = 11)
plt.text(-0.75, -0.46, 'Point mutation rate', fontsize = 11)
plt.text(-0.75, -0.24, 'Small insertion rate', fontsize = 11)
plt.text(-0.75, -0.02, 'Small deletion rate', fontsize = 11)
plt.text(-0.18, 0.3, 'Mutation Rates', fontsize = 18)

plt.plot(0,0)
interactFig.show()

# ------- Fitness and genome size of best indiv through generations figure
evolByhGeneFig = plt.figure(3, figsize=(10,7))
plt.ion()

generation_x = []
fitness_y = []
gen_len_y = []
# Initialization of the plot
(evolByhGeneFig, ax_fitByGene, ax_lenByGene)=vs.init_fitLenByGeneration(evolByhGeneFig, PyInteraction.get_BestIndivFitness(interact), length ,generation_x, fitness_y, gen_len_y)

# -------------------------------------------------------- Simulation Loop
try:
    while(True):

        # Compute the json datas in python arrays
        # 0.1% 
        repartition_array = vs.compute_array_repartition(repartition_fic_name)
        (target, point_x, point_y) = vs.compute_array_fitness_evolution(evolution_fic_name)
        (length, leading_prots, lagging_prots) = vs.compute_array_proteins(proteins_fic_name)
        (length, leading_rnas, lagging_rnas) = vs.compute_array_rnas(rnas_fic_name)

        if (not pause):

            # Compute a new generation
            # 32% of the total time
            PyInteraction.runAStep(interact)
            i+=1
            print("Generation",i)

            # Update the figure of fitness and genome size through the generations
            # 15% of the total time
            (evolByhGeneFig, ax_fitByGene, ax_lenByGene)=vs.update_fitLenByGeneration(evolByhGeneFig, ax_fitByGene, ax_lenByGene, PyInteraction.get_BestIndivFitness(interact), length, generation_x, fitness_y, gen_len_y)
            
            # Update the texts of the plot
            # 0.14% of the total time
            txt_nbgeneration.set_visible(False)
            txt_nbgeneration = ax_nbgeneration.text(x_big_tx+0.44, y_big_tx, str(i), fontsize = 18)
            txt_fitness.set_visible(False)
            txt_fitness = ax_fitness.text(x_big_tx+0.78, y_big_tx, str(PyInteraction.get_BestIndivFitness(interact)), fontsize = 14)
            txt_length.set_visible(False)
            txt_length = ax_length.text(0.8, 0.1, str(length), fontsize = 11)
        
        if(display==False):
            if(i%2==0):     # Display only even generation for saving time
                
                # Update the plots on the main figure
                # 0.19% of the total time
                im=vs.update_repartition(fig, im, repartition_array)
                # 0.09% of the total time
                vs.update_fitness_evolution(fig, point_x, point_y)
                # 0.31% of the total time
                vs.update_proteins(fig,length, leading_prots, lagging_prots)
                # 20% of the total time
                vs.update_rnas(fig, length, leading_rnas, lagging_rnas)
            
                # Allow the figure to being updated
                fig.canvas.draw_idle()
                fig.canvas.flush_events()
            
        if display:
            # Check if the user clicked on a button
            fig.canvas.get_tk_widget().update()

except KeyboardInterrupt:
    print("exit")
