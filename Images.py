# ************************************************************************
#                          Images  -  Main
# ************************************************************************

# ---------------------------------------------------------------- INCLUDE
import Visualization as vs
import Save as sv
from cython_Interaction import PyInteraction

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
import sys
from pathlib import Path

# matplotlib embedded on a tkinter window (to manage the buttons)
matplotlib.use("TkAgg")

# ------------------------------------------------------------------- MAIN

# Instanciate the Interaction object that communicate with the c++ wrapped
# module (glue with cython)
interact = PyInteraction()

# Path of the parameters json file of the simulation 
parameters_file = sys.argv[1]


if(len(sys.argv)==4 and sys.argv[2]=='-o'):
    save_path = sys.argv[3]
else:
    save_path = './Image/'
Path(save_path).mkdir(parents=True, exist_ok=True)

# Instanciate the differents objects for the simulation
PyInteraction.setUp(interact, parameters_file.encode('UTF-8'), 100, 0)

# Path of the json file to read in order to plot the differents graphics
repartition_fic_name = './json/repartition_fic.json'
evolution_fic_name = './json/fitness_evolution_fic.json'
proteins_fic_name = './json/proteins_fic.json'
rnas_fic_name = './json/rnas_fic.json'

# ----------Initialisation of the main figure and the axes of the subplots

fig, ax=plt.subplots(2, 2, figsize=(20,10), gridspec_kw={'height_ratios': [1, 1.5]})
fig.subplots_adjust(left=0.0, bottom=0.05, right=0.95, top=0.89, wspace=0.1, hspace=0.15)
plt.ion()

# Initialisation of the differents graphics
repartition_array = vs.compute_array_repartition(repartition_fic_name)
(im)=vs.init_repartition(repartition_array, fig)

(target, point_x, point_y) = vs.compute_array_fitness_evolution(evolution_fic_name)
vs.init_fitness_evolution(fig, target, point_x, point_y)

(length, leading_prots, lagging_prots) = vs.compute_array_proteins(proteins_fic_name)
vs.init_proteins(fig, length, leading_prots, lagging_prots)

(length, leading_rnas, lagging_rnas) = vs.compute_array_rnas(rnas_fic_name)
vs.init_rnas(fig, length, leading_rnas, lagging_rnas)

# Fitness of the best individual text
ax_fitness = fig.add_axes([0.15, 0, 0.2, 0.1])
ax_fitness.axis('off')
ax_fitness.text(0.3, 0.35, 'Fitness of the best indiv is', fontsize = 14)
txt_fitness = ax_fitness.text(1.08, 0.35, str(PyInteraction.get_BestIndivFitness(interact)), fontsize = 14)

# Size of the genome text
ax_length = fig.add_axes([0.15, 0, 0.2, 0.1])
ax_length.axis('off')
ax_length.text(0.3, 0.1, 'Length of genome is', fontsize = 11)
txt_length = ax_length.text(0.8, 0.1, str(length), fontsize = 11)

# -------------------------------------------------------- Simulation Loop

print("Saving figure in "+save_path)
    
fig.savefig(save_path+'/figures.svg', format='svg')
fig.savefig(save_path+'/figures.png', format='png')

sv.saveFitnessEvolutionFromJson('./json/fitness_evolution_fic.json', save_path)
sv.saveProteinsFromJson('./json/proteins_fic.json', save_path)
sv.saveRnasFromJson('./json/rnas_fic.json', save_path)

plt.figure().clear('all')
plt.close('all')
plt.cla()
plt.clf()