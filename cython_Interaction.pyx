# distutils: language = c++
from cpython.version cimport PY_MAJOR_VERSION

cdef extern from "/home/ambre/Bureau/Aevol/aevol/src/libaevol/7/ExpManager_7.h":
    cdef struct MutationParameters :
        double duplication_rate_
        double deletion_rate_
        double translocation_rate_
        double inversion_rate_
        double point_mutation_rate_
        double small_insertion_rate_
        double small_deletion_rate_
        int max_indel_size_
        int min_genome_length_
        int max_genome_length_

cdef extern from "/home/ambre/Bureau/Aevol/aevol/src/libaevol/7/Interaction.h":
    cdef cppclass Interaction:
        Interaction() except +
        long t_end, t_begin
        void setup (char* input_file_name, long t_end, long t_begin)
        void run_a_step()
        void change_mutation_rate(double duplication_rate, double deletion_rate, double translocation_rate, double inversion_rate, double point_mutation_rate, double small_insertion_rate, double small_deletion_rate )              
        MutationParameters get_mutation_rate()
        double getBestIndivFitness()

cdef class PyInteraction:
    cdef Interaction *c_inter

    def __cinit__ (self):
        self.c_inter= new Interaction()
        if self.c_inter == NULL:
            raise MemoryError('Not enough memory.')
    
    def setUp(self, char* input_file_name, long t_end, long t_begin):
        self.c_inter.setup(input_file_name, t_end, t_begin)

    def runAStep(self):
        self.c_inter.run_a_step()

    def changeMutationRate(self, double duplication_rate, double deletion_rate, double translocation_rate, double inversion_rate, double point_mutation_rate, double small_insertion_rate, double small_deletion_rate ):
        self.c_inter.change_mutation_rate( duplication_rate, deletion_rate, translocation_rate, inversion_rate, point_mutation_rate, small_insertion_rate, small_deletion_rate )
    
    def get_mutation_rate( self ):
        return self.c_inter.get_mutation_rate()

    def get_BestIndivFitness( self ):
        return self.c_inter.getBestIndivFitness()
    
    # Attribute access
    @property
    def t_end(self):
        return self.c_inter.t_end
    # Attribute access
    @property
    def t_begin(self):
        return self.c_inter.t_begin
        
