# Aevol Graphical User Interface - Data visualization

This Python extension to the classical Aevol program enable to visualize on different graphics the data the simulation generates.

The GUI presents three windows. The first one shows four different graphics about the population of individuals, the fitness and the genomes of the best individual. The second one allows to change the different mutation parameters present in the simulation. The last window shows the curves of the evolution of the fitness and the genome size of the best individual of the simulation through the generations.

## Pre-requisit
### The required C++ libraries:
Here are the basic dependencies on a Debian system:
  build-essential
  cmake
  zlib1g-dev
  libboost-dev
  libx11-dev
  libboost-filesystem1.67-dev
Those are the same as the classical Aevol dependencies.
One option to install them all at once is to run the following command:
```shell
sudo apt install build-essential cmake zlib1g-dev libboost-dev libx11-dev libboost-filesystem1.67-dev
```
You need to have the classical Aevol project installed on your computer to run the GUI.

### The required Python libraries:
Here are the specifics librairies used by the Python interface:
  Cython 
  matplotlib 
  numpy 

You can install them all by running the following command:
```shell
pip install -r requirements.txt
```

## How to compile?
### On the C++ side:
In the aevol project run those following commands to compile the shared library:
```shell
mkdir build
cd build
cmake .. -Bshared -DBUILD_SHARED_LIBS=ON
cd shared
make aevol 
```
This shared library '.so' will appears in your aevol project in 'aevol/build/shared/src/libaevol/libaevol.so'

### On the Python side:
In the Python project run this following command line:
```shell
python3 setup.py build_ext --inplace
```
According to your working environment you will need to change some paths in the code. 
In setup.py you will need to change the variable 'aevol_path' by your path to the aevol project. 
In cython_Interaction.pyx there is two paths to change (the string in the 'cdef extern from'). Replace "/home/ambre/Bureau/Aevol/aevol/" by your own path to the aevol project (the same as in the setup.py).

## How to run an experiment?
To run a experiment with the real time data visualization assuming you are in the python project Aevol_visualization you need to run the following commands lines:
```shell
python3 ihm.py PARAMETERS_FILE.json [-o OUTPUT_DIR]
```
PARAMETERS_FILE is a required json file with an initial genome and other parameters. Those are the same than the needed ones in the classical Aevol. 
OUTPUT_DIR is an optional directory where the .png and .svg output graphics images will be saved. 

An other feature allows to just save the images without doing any mutations since a given parameters file.
```shell
python3 Images.py PARAMETERS_FILE.json [-o OUTPUT_DIR]
```
The options are the same as above.

## Differents Buttons on the GUI
You will notice three different buttons on the graphical interface. 'Pause' allows to put the whole program in pause (the computing part and the visualization part).'Display visualization' allows to put on pause only the visualization and let the computation work. If you want to quit correctly the simulation and saving the figures click on 'Quit'.

Where to find the program:
https://gitlab.inria.fr/amychals/aevol_visualization


Visit www.aevol.fr for more information.

