# ************************************************************************
#                          Listener  -  realization
# ************************************************************************
# A class that manage the changes of mutation rates when interacting with 
# the buttons
# ---------------------------------------------------------------- INCLUDE
from cython_Interaction import PyInteraction
import matplotlib.pyplot as plt

# Manage the differents types of mutation rates incrementations
def incrInteract(interaction, type, txt, axe):
    def increaseDup(event):
        print("increase duplication rate")
        actual_rate = get_DuplicationRate(interaction)
        actual_rate =actual_rate*(10)
        PyInteraction.changeMutationRate(interaction,actual_rate, get_DeletionRate(interaction), get_TranslocationRate(interaction), get_InvertionRate(interaction), get_PointMutationRate(interaction), get_SmallInsertionRate(interaction), get_SmallDeletionRate(interaction))
        for tx in axe.texts :
            if tx==txt[0]:
                tx.set_visible(False)
        txt[0] = axe.text(0.2, 0.01, s=str(get_DuplicationRate(interaction)), fontsize = 11)
        
    def increaseDel(event):
        print("increase deletion rate")
        actual_rate = get_DeletionRate(interaction)
        actual_rate =actual_rate*(10)
        PyInteraction.changeMutationRate(interaction, get_DuplicationRate(interaction), actual_rate, get_TranslocationRate(interaction), get_InvertionRate(interaction), get_PointMutationRate(interaction), get_SmallInsertionRate(interaction), get_SmallDeletionRate(interaction))
        for tx in axe.texts :
            if tx==txt[1]:
                tx.set_visible(False)
        txt[1] = axe.text(0.2, 0.17, s=str(get_DeletionRate(interaction)), fontsize = 11)

    def increaseTrans(event):
        print("increase translocation rate")
        actual_rate = get_TranslocationRate(interaction)
        actual_rate =actual_rate*(10)
        PyInteraction.changeMutationRate(interaction, get_DuplicationRate(interaction), get_DeletionRate(interaction), actual_rate, get_InvertionRate(interaction), get_PointMutationRate(interaction), get_SmallInsertionRate(interaction), get_SmallDeletionRate(interaction))
        for tx in axe.texts :
            if tx==txt[2]:
                tx.set_visible(False)
        txt[2] = axe.text(0.2, 0.32, s=str(get_TranslocationRate(interaction)), fontsize = 11)

    def increaseInvert(event):
        print("increase invertion rate")
        actual_rate = get_InvertionRate(interaction)
        actual_rate =actual_rate*(10)
        PyInteraction.changeMutationRate(interaction, get_DuplicationRate(interaction), get_DeletionRate(interaction), get_TranslocationRate(interaction), actual_rate, get_PointMutationRate(interaction), get_SmallInsertionRate(interaction), get_SmallDeletionRate(interaction))
        for tx in axe.texts :
            if tx==txt[3]:
                tx.set_visible(False)
        txt[3] = axe.text(0.2, 0.48, s=str(get_InvertionRate(interaction)), fontsize = 11)

    def increasePoint(event):
        print("increase point mutation rate")
        actual_rate = get_PointMutationRate(interaction)
        actual_rate =actual_rate*(10)
        PyInteraction.changeMutationRate(interaction, get_DuplicationRate(interaction), get_DeletionRate(interaction), get_TranslocationRate(interaction), get_InvertionRate(interaction), actual_rate, get_SmallInsertionRate(interaction), get_SmallDeletionRate(interaction))
        for tx in axe.texts :
            if tx==txt[4]:
                tx.set_visible(False)
        txt[4] = axe.text(0.2, 0.63, s=str(get_PointMutationRate(interaction)), fontsize = 11)

    def increaseSmIns(event):
        print("increase small insertion rate")
        actual_rate = get_SmallInsertionRate(interaction)
        actual_rate =actual_rate*(10)
        PyInteraction.changeMutationRate(interaction, get_DuplicationRate(interaction), get_DeletionRate(interaction), get_TranslocationRate(interaction), get_InvertionRate(interaction), get_PointMutationRate(interaction), actual_rate, get_SmallDeletionRate(interaction))
        for tx in axe.texts :
            if tx==txt[5]:
                tx.set_visible(False)
        txt[5] = axe.text(0.2, 0.78, s=str(get_SmallInsertionRate(interaction)), fontsize = 11)

    def increaseSmDel(event):
        print("increase small deletion rate")
        actual_rate = get_SmallDeletionRate(interaction)
        actual_rate =actual_rate*(10)
        PyInteraction.changeMutationRate(interaction, get_DuplicationRate(interaction), get_DeletionRate(interaction), get_TranslocationRate(interaction), get_InvertionRate(interaction), get_PointMutationRate(interaction), get_SmallInsertionRate(interaction), actual_rate)
        for tx in axe.texts :
            if tx==txt[6]:
                tx.set_visible(False)
        txt[6] = axe.text(0.2, 0.94, s=str(get_SmallDeletionRate(interaction)), fontsize = 11)
    
    if(type=="dup"):
        return increaseDup
    if(type=="del"):
        return increaseDel
    if(type=="trans"):
        return increaseTrans
    if(type=="inv"):
        return increaseInvert
    if(type=="point"):
        return increasePoint
    if(type=="smins"):
        return increaseSmIns
    if(type=="smdel"):
        return increaseSmDel

# Manage the differents types of mutation rates decrementation
def decrInteract(interaction, type, txt, axe):
    def decreaseDup(event):
        print("decrease duplication rate")
        actual_rate = get_DuplicationRate(interaction)
        actual_rate =actual_rate*(0.1)
        PyInteraction.changeMutationRate(interaction,actual_rate, get_DeletionRate(interaction), get_TranslocationRate(interaction), get_InvertionRate(interaction), get_PointMutationRate(interaction), get_SmallInsertionRate(interaction), get_SmallDeletionRate(interaction))
        for tx in axe.texts :
            if tx==txt[0]:
                tx.set_visible(False)
        txt[0] = axe.text(0.2, 0.01, s=str(get_DuplicationRate(interaction)), fontsize = 11)

    def decreaseDel(event):
        print("decrease deletion rate")
        actual_rate = get_DeletionRate(interaction)
        actual_rate =actual_rate*(0.1)
        PyInteraction.changeMutationRate(interaction, get_DuplicationRate(interaction), actual_rate, get_TranslocationRate(interaction), get_InvertionRate(interaction), get_PointMutationRate(interaction), get_SmallInsertionRate(interaction), get_SmallDeletionRate(interaction))
        for tx in axe.texts :
            if tx==txt[1]:
                tx.set_visible(False)
        txt[1] = axe.text(0.2, 0.17, s=str(get_DeletionRate(interaction)), fontsize = 11)

    def decreaseTrans(event):
        print("decrease translocation rate")
        actual_rate = get_TranslocationRate(interaction)
        actual_rate =actual_rate*(0.1)
        PyInteraction.changeMutationRate(interaction, get_DuplicationRate(interaction), get_DeletionRate(interaction), actual_rate, get_InvertionRate(interaction), get_PointMutationRate(interaction), get_SmallInsertionRate(interaction), get_SmallDeletionRate(interaction))
        for tx in axe.texts :
            if tx==txt[2]:
                tx.set_visible(False)
        txt[2] = axe.text(0.2, 0.32, s=str(get_TranslocationRate(interaction)), fontsize = 11)
    
    def decreaseInvert(event):
        print("decrease invertion rate")
        actual_rate = get_InvertionRate(interaction)
        actual_rate =actual_rate*(0.1)
        PyInteraction.changeMutationRate(interaction, get_DuplicationRate(interaction), get_DeletionRate(interaction), get_TranslocationRate(interaction), actual_rate, get_PointMutationRate(interaction), get_SmallInsertionRate(interaction), get_SmallDeletionRate(interaction))
        for tx in axe.texts :
            if tx==txt[3]:
                tx.set_visible(False)
        txt[3] = axe.text(0.2, 0.48, s=str(get_InvertionRate(interaction)), fontsize = 11)
    
    def decreasePoint(event):
        print("decrease point mutation rate")
        actual_rate = get_PointMutationRate(interaction)
        actual_rate =actual_rate*(0.1)
        PyInteraction.changeMutationRate(interaction, get_DuplicationRate(interaction), get_DeletionRate(interaction), get_TranslocationRate(interaction), get_InvertionRate(interaction), actual_rate, get_SmallInsertionRate(interaction), get_SmallDeletionRate(interaction))
        for tx in axe.texts :
            if tx==txt[4]:
                tx.set_visible(False)
        txt[4] = axe.text(0.2, 0.63, s=str(get_PointMutationRate(interaction)), fontsize = 11)
    
    def decreaseSmIns(event):
        print("decrease small insertion rate")
        actual_rate = get_SmallInsertionRate(interaction)
        actual_rate =actual_rate*(0.1)
        PyInteraction.changeMutationRate(interaction, get_DuplicationRate(interaction), get_DeletionRate(interaction), get_TranslocationRate(interaction), get_InvertionRate(interaction), get_PointMutationRate(interaction), actual_rate, get_SmallDeletionRate(interaction))
        for tx in axe.texts :
            if tx==txt[5]:
                tx.set_visible(False)
        txt[5] = axe.text(0.2, 0.78, s=str(get_SmallInsertionRate(interaction)), fontsize = 11)
    
    def decreaseSmDel(event):
        print("decrease small deletion rate")
        actual_rate = get_SmallDeletionRate(interaction)
        actual_rate =actual_rate*(0.1)
        PyInteraction.changeMutationRate(interaction, get_DuplicationRate(interaction), get_DeletionRate(interaction), get_TranslocationRate(interaction), get_InvertionRate(interaction), get_PointMutationRate(interaction), get_SmallInsertionRate(interaction), actual_rate)
        for tx in axe.texts :
            if tx==txt[6]:
                tx.set_visible(False)
        txt[6] = axe.text(0.2, 0.94, s=str(get_SmallDeletionRate(interaction)), fontsize = 11)

    if(type=="dup"):
        return decreaseDup
    if(type=="del"):
        return decreaseDel
    if(type=="trans"):
        return decreaseTrans
    if(type=="inv"):
        return decreaseInvert
    if(type=="point"):
        return decreasePoint
    if(type=="smins"):
        return decreaseSmIns
    if(type=="smdel"):
        return decreaseSmDel

# Return the current differents mutations rates from the simulation
def get_DuplicationRate(interaction):
    muts_rate = PyInteraction.get_mutation_rate(interaction)
    return(muts_rate["duplication_rate_"])
def get_DeletionRate(interaction):
    muts_rate = PyInteraction.get_mutation_rate(interaction)
    return(muts_rate["deletion_rate_"])
def get_TranslocationRate(interaction):
    muts_rate = PyInteraction.get_mutation_rate(interaction)
    return(muts_rate["translocation_rate_"])
def get_InvertionRate(interaction):
    muts_rate = PyInteraction.get_mutation_rate(interaction)
    return(muts_rate["inversion_rate_"])
def get_PointMutationRate(interaction):
    muts_rate = PyInteraction.get_mutation_rate(interaction)
    return(muts_rate["point_mutation_rate_"])
def get_SmallInsertionRate(interaction):
    muts_rate = PyInteraction.get_mutation_rate(interaction)
    return(muts_rate["small_insertion_rate_"])
def get_SmallDeletionRate(interaction):
    muts_rate = PyInteraction.get_mutation_rate(interaction)
    return(muts_rate["small_deletion_rate_"])

