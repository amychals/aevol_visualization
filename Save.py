# ************************************************************************
#                          Save  -  realization
# ************************************************************************
# A file that save in a specified format the 3 main plots of the GUI :
# the fitness landscape, the proteins and the rnas circles.
# ---------------------------------------------------------------- INCLUDE
import Visualization as vs

from matplotlib.transforms import Bbox
import matplotlib.pyplot as plt
import numpy as np 
from matplotlib.patches import Circle, Arc
from mpl_toolkits.axes_grid1 import make_axes_locatable
from pathlib import Path

# Save the fitness landscape at the generation it is called in .svg and .png
def saveFitnessEvolutionFromJson(json_fic, save_path):

    (target, point_x, point_y) = vs.compute_array_fitness_evolution(json_fic)
    plt.ion()
    fig=plt.figure()

    x_axis = np.arange(0, 1, 0.01)
    ax = fig.add_subplot(111)
    target_curve=[0]*len(x_axis)

    for i in range (len(target)):
        target_curve += vs.gaussian(target[i][0], target[i][1],target[i][2], x_axis)

    ax.set_facecolor((1,1,1,1))
    line1, =ax.plot(x_axis, np.clip(target_curve, 0, 1), color="red", label="environment")
    line2, =ax.plot(point_x, point_y, color="black", alpha=0.75, label="fitness of best individual")
    ax.legend(handles=[line1, line2], loc="upper left")
    
    im=plt.show()
    divider = make_axes_locatable(ax)
    cax = divider.new_vertical(size="5%", pad=0.25, pack_start=True)
    fig.add_axes(cax)
    cbar=fig.colorbar(im, cax=cax, orientation="horizontal", cmap='gist_rainbow')
    cbar.set_ticks([])

    Path(save_path).mkdir(parents=True, exist_ok=True)

    fig.savefig(save_path+'/SaveFitness.svg', format='svg')
    fig.savefig(save_path+'/SaveFitness.png', format='png')

# Save the proteins circle at the generation it is called in .svg and .png
def saveProteinsFromJson(json_fic, save_path):
    (length, leading_prots, lagging_prots) = vs.compute_array_proteins(json_fic)
    fig=plt.figure()
    ax = fig.add_subplot(111)
    (arcs_leading_matrix, arcs_lagging_matrix, u, v)=vs.compute_arc_matrix(length, leading_prots, lagging_prots)
    for arcs in arcs_leading_matrix:
        ax.add_patch(arcs)

    for arcs in arcs_lagging_matrix:
        ax.add_patch(arcs)
    
    # scale
    scale = [0, 0]
    (scale)=vs.compute_scale(length)
    ax.add_patch(scale[0])
    ax.text(0.88, 0.85, str(scale[1])+" bp", fontdict={'family': 'serif',
        'color':  'black',
        'weight': 'normal',
        'size': 6,
        })

    ax.set_facecolor((1,1,1,1))
    ax.add_patch(Circle((0.5, 0.5), 0.32 , angle=90, fill=False, lw=1, color='black'))

    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    ax.axis('off')
    
    ax.set_aspect(1)
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)

    Path(save_path).mkdir(parents=True, exist_ok=True)

    fig.savefig(save_path+'/SaveProteins.svg', format='svg')
    fig.savefig(save_path+'/SaveProteins.png', format='png')

# Save the rnas circle at the generation it is called in .svg and .png
def saveRnasFromJson(json_fic, save_path):
    (length, leading_rnas, lagging_rnas) = vs.compute_array_rnas(json_fic)
    fig=plt.figure()
    ax = fig.add_subplot(111)
    (arcs_leading_matrix, arcs_lagging_matrix, arcs_ending_leading_matrix, arcs_ending_lagging_matrix)=vs.compute_arc_matrix(length, leading_rnas, lagging_rnas)
    for arcs in arcs_leading_matrix:
        ax.add_patch(arcs)
    for arcs in arcs_ending_leading_matrix:
        ax.add_patch(arcs)
    for arcs in arcs_ending_lagging_matrix:
        ax.add_patch(arcs)
    for arcs in arcs_lagging_matrix:
        ax.add_patch(arcs)

    # scale
    scale = [0, 0]
    (scale)=vs.compute_scale(length)
    ax.add_patch(scale[0])
    ax.text(0.88,0.85, str(scale[1])+" bp", fontdict={'family': 'serif',
        'color':  'black',
        'weight': 'normal',
        'size': 6,
        })

    ax.set_facecolor((1,1,1,1))
    ax.add_patch(Circle((0.5,0.5), 0.32 , angle=90, fill=False, lw=1, color='black'))

    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    ax.axis('off')
    
    ax.set_aspect(1)
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)

    Path(save_path).mkdir(parents=True, exist_ok=True)

    fig.savefig(save_path+'/SaveRnas.svg', format='svg')
    fig.savefig(save_path+'/SaveRnas.png', format='png')