# ************************************************************************
#                          Visualization  -  realization
# ************************************************************************

# ---------------------------------------------------------------- INCLUDE
import JsonToPython as jp

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Circle, Arc
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np  

# ------------------------------------------------------- Global variables
time_t=0
nb_t=0

last_rna_scale=0
last_prot_scale=0

max_tab = 0
min_tab=0
font = {'family': 'serif',
        'color':  'black',
        'weight': 'normal',
        'size': 6,
        }

s_posx = 0.88
s_posy = 0.85

center = (0.5, 0.5)
radius = 0.32

# ---------------------------------------- Repartition plot bind functions

# Compute well formated array from a json file
def compute_array_repartition(fic_name):
    (ncol, indiv_array)=jp.serialize_repartition(fic_name)

    # 1D to 2D array
    indiv_array=np.array(indiv_array)
    indiv_array=np.reshape(indiv_array, (-1, ncol))

    return(indiv_array)
    
# Initialize the repartition of fitness in population grid
def init_repartition(indiv_array, fig):
    # call the global variables
    global max_tab
    global min_tab

    max_tab = indiv_array[0][0]
    min_tab=indiv_array[0][0]

    # compute the limits for the color scale
    for i in range(len(indiv_array)):
        for j in range(len(indiv_array[0])):
            if(max_tab<indiv_array[i][j]):
                max_tab=indiv_array[i][j]
            if(min_tab>indiv_array[i][j]):
                min_tab=indiv_array[i][j]

    # build the heatmap with logarithmic scale
    im = (fig.axes[0]).imshow(indiv_array, norm=LogNorm())
    im.set_clim(vmin=min_tab, vmax=max_tab)  

    # add a colorbar
    fig.colorbar(im, ax=fig.axes[0])

    (fig.axes[0]).set_title('Repartition of individuals')

    return(im)

# Update the heatmap of the fitness repartition
def update_repartition(fig, im, indiv_array):
    # call the global variables
    global max_tab
    global min_tab

    # compute the limits for the color scale
    for i in range(len(indiv_array)):
        for j in range(len(indiv_array[0])):
            if(max_tab<indiv_array[i][j]):
                max_tab=indiv_array[i][j]
            if(min_tab>indiv_array[i][j]):
                min_tab=indiv_array[i][j]

    # update the data with a new fitness of individuals array
    im.set_data(indiv_array)
    im.set_clim(vmin=min_tab, vmax=max_tab)

    del(indiv_array) 

    return(im)

# ---------------------------------- Fitness landscape plot bind functions

# Compute a gaussian curve with given parameters : 
# the height 'H', the coordinate of the center of the curve 'M' on the 
# 'x' axis and the width 'W' of the gaussian 
def gaussian(H, M, W, x):
    res = H*np.exp(-1*pow((x-M),2)/(2*pow(W,2)))
    return res

# Compute a triangular curve with given parameters : 
# the height 'H', the coordinate of the center of the curve 'M' on the 
# 'x' axis and the width 'W' of the triangle (this function isn't use) 
def triangular_function(H, M, W,x):
    res = [0]*len(x)
    a=H/W
    for i in range (len(x)):
        if(M-W<x[i]<M):
            b=H*((W-M)/W)
            res[i]=a*x[i]+b
        elif(M<=x[i]<M+W):
            b=H*((W+M)/W)
            res[i]=-a*x[i]+b
    return res

# Compute well formated array from a json file
def compute_array_fitness_evolution(fic_name):
    (target, point_x, point_y)=jp.serialize_fitness_evolution(fic_name)
    return(target, point_x, point_y)

# Initialize the fitness landscape on a subplot
def init_fitness_evolution(fig, target, point_x, point_y):
    x_axis = np.arange(0, 1, 0.01)
    target_curve=[0]*len(x_axis)

    # compute the environment gaussian curve
    for i in range (len(target)):
        target_curve += gaussian(target[i][0], target[i][1],target[i][2], x_axis)

    # plot the differents curves on the subplot
    (fig.axes[1]).set_facecolor((1,1,1,1))
    line1, =(fig.axes[1]).plot(x_axis, np.clip(target_curve, 0, 1), color="red", label="environment")
    line2, =(fig.axes[1]).plot(point_x, point_y, color="black", alpha=0.75, label="fitness of best individual")
    (fig.axes[1]).legend(handles=[line1, line2], loc="upper left")
        
    im=plt.show()

    # Add a colorbar under the plot
    divider = make_axes_locatable((fig.axes[1]))
    cax = divider.new_vertical(size="5%", pad=0.25, pack_start=True)
    fig.add_axes(cax)
    cbar=fig.colorbar(im, cax=cax, orientation="horizontal", cmap='gist_rainbow')
    cbar.set_ticks([])
    # other option of colorbar : fig.colorbar(im, cax=(fig.axes[1]), orientation='horizontal', cmap='gist_rainbow')

    (fig.axes[1]).set_title('Fitness of best individual in environment ')

# Update the best individual fitness curve 
def update_fitness_evolution(fig, point_x, point_y):
    (fig.axes[1]).lines.pop(1)
    (fig.axes[1]).plot(point_x, point_y, color="black", alpha=0.75, label='fitness of best indiv')

    del(point_x)
    del(point_y)

# -------------------- Proteins and rnas visualization computing functions

# Compute the differents arcs corresponding to a protein or a rna
# element and their distance to the genome circle in order that there  
# is not overlapping between the elements
def compute_arc_matrix(length, leading_elemt, lagging_elemt):
    diam = radius*2
    delta=0.03
    eltColor=0.0
    last_layer=0

    arcs_leading_matrix = [0]*len(leading_elemt)
    arcs_ending_leading_matrix = [0]*len(leading_elemt)
    arcs_lagging_matrix = [0]*len(lagging_elemt)
    arcs_ending_lagging_matrix = [0]*len(lagging_elemt)

    occupied_lead_sectors=[[0]*360 for i in range(len(leading_elemt))]
    # Carol code implementation (cf. Individual_X11.cpp l. 553 (function display rna))
    # The algorithm use an array of 360 cells * maximum number of layers.
    # It compute the angles of each arcs and check if there is enough place on the circle 
    # at this angle location. If not, it adds a layer and check again.
    for led_el in leading_elemt:
        if(led_el[0]==led_el[1]):
            theta1 = round((led_el[0]/length)*360)
            theta2 = round((led_el[1]/length)*360)-2
        else:
            theta1 = round((led_el[0]/length)*360)
            theta2 = round((led_el[1]/length)*360)
        i=0
        
        nb_sectors = round((theta2-theta1+1)%360)
        layer=0
        sector_free = False
        while(not sector_free):
            sector_free=True
            rho=0
            for rho in range (nb_sectors):
                if(occupied_lead_sectors[layer][round((theta1+rho)%360)]!=0):
                    sector_free=False
                    break
            if(sector_free):
                break
            else:
                layer+=1
                if(layer>last_layer):
                    # If this is the first time this layer is used, we are sure it has enough place
                    last_layer+=1
                    break
        for rho in range (nb_sectors):
            occupied_lead_sectors[layer][round((theta1+rho)%360)]=led_el[0]
        led_el[2]=(layer+1)*delta

    del(occupied_lead_sectors)

    # Idem on lagging elements
    last_layer=0
    occupied_lag_sectors=[[0]*360 for i in range(len(lagging_elemt))]
    for lag_el in lagging_elemt:
        if(lag_el[0]==lag_el[1]):
            theta1 = round((lag_el[0]/length)*360)
            theta2 = round((lag_el[1]/length)*360)+2
        else:
            theta1 = round((lag_el[0]/length)*360)
            theta2 = round((lag_el[1]/length)*360)
        i=0
        nb_sectors = round((theta1-theta2+1)%360)
        layer=0
        sector_free = False
        while(not sector_free):
            sector_free=True
            rho=0
            for rho in range (nb_sectors):
                if(occupied_lag_sectors[layer][round((theta1-rho)%360)]!=0):
                    sector_free=False
                    break
            if(sector_free):
                break
            else:
                layer+=1
                if(layer>last_layer):
                    last_layer+=1
                    break
        rho=0
        for rho in range (nb_sectors):
            occupied_lag_sectors[layer][round((theta1-rho)%360)]=lag_el[0]
        lag_el[2]=(layer+1)*(-delta)

    del(occupied_lag_sectors)

    # Compute the differents arcs matrixs to return with the corresponding angles and the
    # corresponding layer.
    # For rnas elements there is the arcs_ending_matrix corresponding to the terminators of
    # a rna strand (a thicker arc at the end of the main rna arc).
    for i in range(len(leading_elemt)):
        theta1 = (leading_elemt[i][0]/length)*360
        theta2 = (leading_elemt[i][1]/length)*360
        theta3 = ((leading_elemt[i][1]-11)/length)*360
        if(leading_elemt[i][0]==leading_elemt[i][1]):
            theta2-=0.0001
            
        if(leading_elemt[i][3]==-1.0) :
            eltColor = "grey"    
        else :
            eltColor = get_RGBA_Color(leading_elemt[i][3])
        arcs_leading_matrix[i]= Arc(center, (diam+leading_elemt[i][2]), (diam+leading_elemt[i][2]), angle=90, theta1=-theta2, theta2=-theta1, linewidth=2, color=eltColor)
        arcs_ending_leading_matrix[i]= Arc(center, (diam+leading_elemt[i][2]), (diam+leading_elemt[i][2]), angle=90, theta1=-theta2, theta2=-theta3+1, linewidth=4, color=eltColor)
    
    for i in range(len(lagging_elemt)):
        theta1 = (lagging_elemt[i][0]/length)*360
        theta2 = (lagging_elemt[i][1]/length)*360
        theta3 = ((lagging_elemt[i][1]+11)/length)*360
        if(lagging_elemt[i][0]==lagging_elemt[i][1]):
            theta2+=0.0001
        if(lagging_elemt[i][3]==-1.0) :
            eltColor = "grey"    
        else :
            eltColor = get_RGBA_Color(lagging_elemt[i][3])

        arcs_lagging_matrix[i]= Arc(center, (diam+lagging_elemt[i][2]), (diam+lagging_elemt[i][2]), angle=90, theta1=-theta1, theta2=-theta2, linewidth=2, color=eltColor)
        arcs_ending_lagging_matrix[i]= Arc(center, (diam+lagging_elemt[i][2]), (diam+lagging_elemt[i][2]), angle=90, theta1=-theta3-1, theta2=-theta2, linewidth=4, color=eltColor)

    del(leading_elemt)
    del(lagging_elemt)
    return (arcs_leading_matrix, arcs_lagging_matrix, arcs_ending_leading_matrix, arcs_ending_lagging_matrix)

# Compute the arc scale corresponding to the given length of the genome 
def compute_scale (length):
    # call the global variables
    global center
    global radius

    scale = 0
    if(length<=10):
        scale=length
    elif(length<=100):
        scale = 10
    elif (length<=1000):
        scale=100
    elif (length<=100000):
        scale = 1000
    else :
        scale = 10000

    theta1_scale = 40
    theta2_scale = ((scale*360)/length)+40
    arc_scale= Arc(center, ((2*radius)+0.35), ((2*radius)+0.35), angle=0, theta1=theta1_scale, theta2=theta2_scale, linewidth=2, color='black')
    
    return(arc_scale, scale)

# Compute the color associated to the abscissa of a protein triangular
# function (M parameter)
def get_RGBA_Color(m_protein):
    cmap = matplotlib.cm.get_cmap('gist_rainbow') 
    rgba = cmap(m_protein)
    return(rgba)

# ---------------------------------- Proteins visualization bind functions

# Compute well formated array from a json file
def compute_array_proteins(fic_name):
    (length, leading_prots, lagging_prots)=jp.serialize_proteins(fic_name)
    # insert a second position a parameter corresponding to the distance 
    # between the protein curve and the genome circle
    for prot in leading_prots:
        prot.insert(2,0.05)
    for prot in lagging_prots:
        prot.insert(2,-0.05)
    return(length, leading_prots, lagging_prots)

# Initialize the proteins circle on a subplot
def init_proteins(fig, length, leading_prots, lagging_prots):
    # call the global variables
    global center
    global radius
    global last_prot_scale

    # add the unmoving genome circle
    (fig.axes[2]).add_patch(Circle(center, radius , angle=90, fill=False, lw=1, color='black'))

    (arcs_leading_matrix, arcs_lagging_matrix, u, v)=compute_arc_matrix(length, leading_prots, lagging_prots)

    for arcs in arcs_leading_matrix:
        (fig.axes[2]).add_patch(arcs)

    for arcs in arcs_lagging_matrix:
        (fig.axes[2]).add_patch(arcs)
    
    # Compute the arc scale
    scale = [0, 0]
    (scale)=compute_scale(length)
    last_prot_scale=scale[1]   

    (fig.axes[2]).add_patch(scale[0])
    (fig.axes[2]).text(s_posx,s_posy, str(scale[1])+" bp", fontdict=font)

    (fig.axes[2]).set_facecolor((1,1,1,1))

    (fig.axes[2]).get_xaxis().set_visible(False)
    (fig.axes[2]).get_yaxis().set_visible(False)
    (fig.axes[2]).axis('off')

    (fig.axes[2]).set_aspect(1)
    (fig.axes[2]).set_xlim(0, 1)
    (fig.axes[2]).set_ylim(0, 1)
    (fig.axes[2]).set_title('Genome and its proteins', y=1.0, pad=0)

    plt.draw()

# Update the proteins around the circle
def update_proteins(fig, length, leading_prots, lagging_prots):
    # call the global variable
    global last_prot_scale
    i=1

    # Remove the old proteins arcs
    while(i<len((fig.axes[2]).patches)):
        (fig.axes[2]).patches.pop(i)

    (arcs_leading_matrix, arcs_lagging_matrix, u, v)=compute_arc_matrix(length, leading_prots, lagging_prots)
    del(leading_prots)
    del(lagging_prots)

    # Add the new computed ones
    for arcs in arcs_leading_matrix:
        (fig.axes[2]).add_patch(arcs)

    for arcs in arcs_lagging_matrix:
        (fig.axes[2]).add_patch(arcs)

    del(arcs_leading_matrix)
    del(arcs_lagging_matrix)
    
    # Compute the arc scale and updating only if it changed
    scale = compute_scale(length)
    
    if(last_prot_scale!=scale[1]):
        del (fig.axes[2]).texts[0]
        last_prot_scale=scale[1]
        (fig.axes[2]).text(s_posx,s_posy, str(scale[1])+" bp", fontdict=font)

    (fig.axes[2]).add_patch(scale[0])

    del(scale)

# -------------------------------------- Rnas visualization bind functions

# Compute well formated array from a json file
def compute_array_rnas(fic_name):
    (length, leading_rnas, lagging_rnas)=jp.serialize_rnas(fic_name)
    for rna in leading_rnas:
        rna.insert(2,0.05)
    for rna in lagging_rnas:
        rna.insert(2,-0.05)
    return(length, leading_rnas, lagging_rnas)

# Initialize the rnas circle on a subplot
def init_rnas(fig, length, leading_rnas, lagging_rnas):
    # call the global variables
    global center
    global radius
    
    # add the unmoving genome circle
    (fig.axes[3]).add_patch(Circle(center, radius , angle=90, fill=False, lw=1, color='black'))

    (arcs_leading_matrix, arcs_lagging_matrix, arcs_ending_leading_matrix, arcs_ending_lagging_matrix)=compute_arc_matrix(length, leading_rnas, lagging_rnas)

    for arcs in arcs_leading_matrix:
        (fig.axes[3]).add_patch(arcs)
    for arcs in arcs_ending_leading_matrix:
        (fig.axes[3]).add_patch(arcs)
    for arcs in arcs_ending_lagging_matrix:
        (fig.axes[3]).add_patch(arcs)
    for arcs in arcs_lagging_matrix:
        (fig.axes[3]).add_patch(arcs)

    # Compute the arc scale
    scale = [0, 0]
    (scale)=compute_scale(length)

    global last_rna_scale
    last_rna_scale=scale[1]

    (fig.axes[3]).add_patch(scale[0])
    (fig.axes[3]).text(s_posx,s_posy, str(scale[1])+" bp", fontdict=font)

    (fig.axes[3]).set_facecolor((1,1,1,1))
    
    (fig.axes[3]).get_xaxis().set_visible(False)
    (fig.axes[3]).get_yaxis().set_visible(False)
    (fig.axes[3]).axis('off')
    
    (fig.axes[3]).set_aspect(1)
    (fig.axes[3]).set_xlim(0, 1)
    (fig.axes[3]).set_ylim(0, 1)
    (fig.axes[3]).set_title('Genome and its rnas', y=1.0, pad=0)
    
    plt.draw()

# Update the rnas strands around the circle
def update_rnas(fig, length, leading_rnas, lagging_rnas):
    # call the global variable
    global last_rna_scale
    i=1

    # Remove the old rnas arcs
    while(i<len((fig.axes[3]).patches)):
        (fig.axes[3]).patches.pop(i)

    (arcs_leading_matrix, arcs_lagging_matrix, arcs_ending_leading_matrix, arcs_ending_lagging_matrix)=compute_arc_matrix(length, leading_rnas, lagging_rnas)
    del(leading_rnas)
    del(lagging_rnas)
    
    # Add the new computed ones
    for arcs in arcs_leading_matrix:
        (fig.axes[3]).add_patch(arcs)
    for arcs in arcs_ending_leading_matrix:
        (fig.axes[3]).add_patch(arcs)
    for arcs in arcs_ending_lagging_matrix:
        (fig.axes[3]).add_patch(arcs)
    for arcs in arcs_lagging_matrix:
        (fig.axes[3]).add_patch(arcs)

    del(arcs_leading_matrix)
    del(arcs_ending_leading_matrix)
    del(arcs_ending_lagging_matrix)
    del(arcs_lagging_matrix)

    # Compute the arc scale and updating only if it changed
    scale = compute_scale(length)

    if(last_rna_scale!=scale[1]):
        del (fig.axes[3]).texts[0]
        last_rna_scale=scale[1]
        (fig.axes[3]).text(s_posx,s_posy, str(scale[1])+" bp", fontdict=font)
    
    (fig.axes[3]).add_patch(scale[0])

    del(scale)

# ---------------------------- fitness and genome size plot bind functions

# Initialize the fitness and genome size evolution plot (on a new 
# window)
def init_fitLenByGeneration (fig, new_fitness, new_gen_len,  generation_x, fitness_y, gen_len_y):
    ax = fig.add_subplot(111)
    ax_len = ax.twinx()

    new_gene = len(generation_x)
    generation_x.append(new_gene)
    fitness_y.append(new_fitness)
    gen_len_y.append(new_gen_len)

    ax.set_facecolor((1,1,1,1))

    line1, =ax.plot(generation_x, fitness_y, color="blue", label='fitness')

    ax_text=fig.add_axes([0.05, 0.05, 0.9, 0.9])

    ax_text.axis('off')
    ax_text.text(0.15, 0.97,'Best individual fitness evolution through generations', fontsize = 18)

    ax_text.text(0.0, 0.95,'fitness', fontsize = 12)
    ax_text.text(0.45, 0,'generations', fontsize = 12)
    ax_text.text(0.95, 0.95,'genome\n length', fontsize = 12)

    line2, =ax_len.plot(generation_x, gen_len_y, color="green", label='genome size')
    ax_len.legend(handles=[line1, line2], loc="upper left")

    plt.draw()
    return(fig, ax,ax_len)

# Update the fitness and genome size evolution curves
def update_fitLenByGeneration(fig, ax,ax_len, new_fitness, new_gen_len, generation_x, fitness_y, gen_len_y):
    # Remove the old lines
    ax.lines.pop(0)
    ax_len.lines.pop(0)
    
    new_gene = len(generation_x)
    generation_x.append(new_gene)
    fitness_y.append(new_fitness)
    gen_len_y.append(new_gen_len)

    # Plot the new ones
    ax.plot(generation_x, fitness_y, color="blue")
    ax_len.plot(generation_x, gen_len_y, color="green")

    del(generation_x)
    del(fitness_y)
    del(gen_len_y)

    fig.canvas.draw()
    fig.canvas.flush_events()

    return(fig, ax,ax_len)