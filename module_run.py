# ************************************************************************
#                          module_run  -  realization
# ************************************************************************
# A python implementation of the classical c++ module_run (main of Aevol) 
# using the wrapped-in-cython c++ class "Interaction.cpp".
# ---------------------------------------------------------------- INCLUDE
from cython_Interaction import PyInteraction

print("Début de la simulation : ")
interact = PyInteraction()
jsonFile_string = "./json/result.json"
PyInteraction.setUp(interact, jsonFile_string.encode('UTF-8'), 100, 0)

i=0
while(True):
    i+=1
    print("Generation",i+1)
    PyInteraction.runAStep(interact)
